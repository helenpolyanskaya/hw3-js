// 1) Цикли потрібні щоб не дублювати функціі, щоб скоротити код

// 2) В JS використовувала:
// "if" при наданні доступу по обмеженню віку
// "while" нескінченність циклу поки не нададуть необхідну інфо

// 3) явне перетворення, коли співподає тип, не явне коли різні типи: "1"+2=12


let number1 = +prompt ("Say any number...");

while (!(number1%1==0) || number1<1) {
    number1 = +prompt ("Say any number...");
}

for (i=1; i<=number1; i++) {
   
    if (i%5==0 || i%10==0) {
        console.log(i);     
    } 
}

if (number1<5) {
    console.log("Sorry, no numbers");
} 

let number2 = +prompt ("Say any else number...");
let number3 = +prompt ("Say one more number...");

while (!(number2%1==0) || !(number3%1==0)) {
    number2 = +prompt ("Say any else number...");
    number3 = +prompt ("Say one more number...");
}

let m = number2;
let n = number3;

if (number2>number3) {
    m = number3;
    n = number2;
}

nextPrime:
for (i=m; i<=n; i++) {
    
    for ( a=2; a<i-1; a++ )

        if (i%a==0) {
            continue nextPrime;              
    }   

    console.log(i);    
}

        
